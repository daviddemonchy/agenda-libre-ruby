# Preview all emails at http://localhost:3000/rails/mailers/orga_mailer
class OrgaMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/orga_mailer/create
  def create
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    OrgaMailer.create Orga.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/orga_mailer/update
  def update
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    orga = Orga.last
    orga.name += ' et hop'
    orga.save!
    OrgaMailer.update orga
  end

  # Preview this email at http://localhost:3000/rails/mailers/orga_mailer/accept
  def accept
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    OrgaMailer.accept Orga.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/orga_mailer/destroy
  def destroy
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    orga = Orga.last
    # orga.reason = 'hello world'
    OrgaMailer.destroy orga
  end
end
