(function() {
  $(document).ready(function() {
    return tinyMCE.init({
      schema: 'html5',
      menubar: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-80267254eb940edca206faebfaaef536c10bb76d2332249a101cb05f18e6f833.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      toolbar: [' bold italic strikethrough | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | link image media insertdatetime charmap table | undo redo | searchreplace | code visualblocks preview fullscreen'],
      plugins: 'lists, advlist, autolink, link, image, charmap, paste, print, preview, table, fullscreen, searchreplace, media, insertdatetime, visualblocks, visualchars, wordcount, contextmenu, code'
    });
  });

  $(document).on('page:receive', function() {
    return tinymce.remove();
  });

}).call(this);
