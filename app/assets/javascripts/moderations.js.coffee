$(document).on 'turbolinks:load', ->
  $('body.moderations .field.closer input[type=radio]').click ->
    $('body.moderations #event_reason').parent().slideUp()
  $('body.moderations .field.opener input[type=radio]').click ->
    $('body.moderations #event_reason').parent().slideDown()
